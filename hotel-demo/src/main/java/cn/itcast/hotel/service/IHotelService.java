package cn.itcast.hotel.service;

import cn.itcast.hotel.pojo.Hotel;
import cn.itcast.hotel.pojo.HotelDoc;
import cn.itcast.hotel.pojo.qo.PageParams;
import cn.itcast.hotel.pojo.vo.PageResult;
import com.baomidou.mybatisplus.extension.service.IService;

public interface IHotelService extends IService<Hotel> {

    /**
     * 酒店的数据检索
     * @param pageParams 分页查询对象
     * @return 返回分页结果
     */
    PageResult<HotelDoc> search(PageParams pageParams);
}
