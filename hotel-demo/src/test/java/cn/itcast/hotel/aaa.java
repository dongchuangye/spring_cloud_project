package cn.itcast.hotel;

import cn.itcast.hotel.pojo.HotelDoc;
import com.alibaba.fastjson.JSON;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.sort.SortOrder;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;

@SpringBootTest
//@RequiredArgsConstructor
@Slf4j
public class aaa {

//    private final RestHighLevelClient client;
    @Autowired
    private  RestHighLevelClient client;

    @Test
    public void testMatchAll() throws IOException {
//        发起请求，创建SearchRequest对象，指定索引苦命
        SearchRequest searchRequest = new SearchRequest("hotel");
//        构建dsl参数
        SearchSourceBuilder searchSourceBuilder = searchRequest.source();
        // 查询全部
       searchSourceBuilder.query(QueryBuilders.matchAllQuery());
        // 发送client请求
        SearchResponse response = client.search(searchRequest, RequestOptions.DEFAULT);
        handleResponse(response);
    }

    /**
     * match查询
     */
    @Test
    public void testMatch() throws IOException {
//        创建对象
        SearchRequest searchRequest = new SearchRequest("hotel");
        searchRequest.source().query(QueryBuilders.matchQuery("all","如家"));
        SearchResponse response = client.search(searchRequest, RequestOptions.DEFAULT);
        handleResponse(response);
    }

    /**
     * 精确查询
     */
    @Test
    public void testJq() throws IOException {
        SearchRequest searchRequest = new SearchRequest("hotel");
        searchRequest.source().query(QueryBuilders.termQuery("city","上海"));
        searchRequest.source().query(QueryBuilders.rangeQuery("price").gte(100).lte(500));
        SearchResponse response = client.search(searchRequest, RequestOptions.DEFAULT);
        handleResponse(response);

    }

    /**
     * 布尔查询你
     * @throws IOException
     */
    @Test
    public void testBool() throws IOException{
        SearchRequest searchRequest = new SearchRequest("hotel");
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        boolQueryBuilder.must(QueryBuilders.termQuery("city","上海"));
        boolQueryBuilder.filter(QueryBuilders.rangeQuery("price").gte(400));
     //   boolQueryBuilder.filter(QueryBuilders.geoDistanceQuery("distance").distance("10km"));

        searchRequest.source().query(boolQueryBuilder);
        SearchResponse response = client.search(searchRequest, RequestOptions.DEFAULT);
        handleResponse(response);
    }

    /**
     * 分页查询
     * @@throws IOException
     */
    @Test
    public void testPageAndSort() throws IOException{
        SearchRequest searchRequest = new SearchRequest("hotel");
        searchRequest.source().query(QueryBuilders.matchAllQuery());
        searchRequest.source().from(0).size(10);
        searchRequest.source().sort("price", SortOrder.ASC);
        SearchResponse response = client.search(searchRequest, RequestOptions.DEFAULT);
        handleResponse(response);
    }
    @Test
    public void testHighlight() throws IOException{
        SearchRequest searchRequest = new SearchRequest("hotel");
        searchRequest.source().query(QueryBuilders.matchQuery("all","如家"));
        HighlightBuilder highlightBuilder = new HighlightBuilder();
        highlightBuilder
        .field("name")
        .preTags("<em>")
        .postTags("</em>")
        .requireFieldMatch(false);
        searchRequest.source().highlighter(highlightBuilder);
        SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
       // 高亮解析
        SearchHits searchResponseHits = searchResponse.getHits();
        long value = searchResponseHits.getTotalHits().value;
        SearchHit[] hits = searchResponseHits.getHits();
        for (SearchHit hit : hits) {
            String id = hit.getId();
            System.out.println("文档id为:"+id);
            String json = hit.getSourceAsString();
            System.out.println(json);
            HotelDoc hotelDoc = JSON.parseObject(json, HotelDoc.class);
            Map<String, HighlightField> highlightFields = hit.getHighlightFields();
            HighlightField highlightField = highlightFields.get("name");
            Text[] fragments = highlightField.getFragments();

            StringBuilder stringBuilder = new StringBuilder();
            for (Text fragment : fragments) {
                stringBuilder.append(fragment);
            }
            System.out.println("获得高亮数据："+stringBuilder);

//            if (!CollectionUtils.isEmpty(highlightFields)){
//                HighlightField highlightField = highlightFields.get("name");
//                if (highlightField != null){
//                    String name = highlightField.getFragments()[0].string();
//                    hotelDoc.setName(name);
//                }
//            }
//           System.out.println("获得高亮数据："+hotelDoc);
        }


    }




    private void handleResponse(SearchResponse response) {
        // 解析结果
        SearchHits searchHits = response.getHits();
        // 拿到总条数
        long value = searchHits.getTotalHits().value;
        // 拿到hits中的hits
        SearchHit[] searchHitsHits = searchHits.getHits();
        for (SearchHit searchHitsHit : searchHitsHits) {
//            获取socre
            String json = searchHitsHit.getSourceAsString();
            // 反序列化
            HotelDoc hotelDoc = JSON.parseObject(json, HotelDoc.class);
            log.info("获取数据：{}", hotelDoc);
        }
    }
}
