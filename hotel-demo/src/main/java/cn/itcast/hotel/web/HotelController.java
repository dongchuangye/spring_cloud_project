package cn.itcast.hotel.web;

import cn.itcast.hotel.pojo.HotelDoc;
import cn.itcast.hotel.pojo.qo.PageParams;
import cn.itcast.hotel.pojo.vo.PageResult;
import cn.itcast.hotel.service.impl.HotelService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
public class HotelController {

    private final HotelService hotelService;
    @PostMapping("/hotel/list")
    public PageResult<HotelDoc> search(@RequestBody PageParams pageParams){
          return hotelService.search(pageParams);
    }
}

// 提交全部代码
