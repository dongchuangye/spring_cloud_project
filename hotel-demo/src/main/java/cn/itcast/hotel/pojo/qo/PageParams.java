package cn.itcast.hotel.pojo.qo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PageParams {
    private String key;
    private int page;
    private int size;
    private String sortBy;

    private  String brand;
    private  String city;
    private  Integer maxPrice;
    private  Integer minPrice;
    private  String starName;
    private  String location;

}
