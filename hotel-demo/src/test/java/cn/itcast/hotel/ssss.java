package cn.itcast.hotel;

import cn.itcast.hotel.constants.HotelConstants;
import cn.itcast.hotel.pojo.Hotel;
import cn.itcast.hotel.pojo.HotelDoc;
import cn.itcast.hotel.service.impl.HotelService;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.api.R;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.xcontent.XContentType;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.List;

@SpringBootTest
public class ssss {

    @Autowired
    private RestHighLevelClient client;

    @Autowired
    private HotelService hotelService;



    @Test
    void createHotelIndex() throws IOException {
        // 1.创建requset对象
        CreateIndexRequest createIndexRequest = new CreateIndexRequest("hotel");

        // 2 准备请求参数 DSL语句
        createIndexRequest.source(HotelConstants.MAPPING_TEMPLATE, XContentType.JSON);
        // 3. 发送请求
        client.indices().create(createIndexRequest, RequestOptions.DEFAULT);

    }
    @Test
    void testDeleteHotelIndex() throws IOException {
        DeleteIndexRequest deleteIndexRequest = new DeleteIndexRequest("hotel");

        client.indices().delete(deleteIndexRequest,RequestOptions.DEFAULT);
    }
    @Test
    void testExistsHotelIndex() throws IOException {
        GetIndexRequest getIndexRequest = new GetIndexRequest("hotel");

        boolean exists = client.indices().exists(getIndexRequest, RequestOptions.DEFAULT);

        System.err.println(exists ? "索引库已经存在！" : "索引库不存在！");

    }

    /**
     * 新增文档
     * @throws IOException
     */
    @Test
    void testAddDocument() throws IOException {
//        根据id查询数据
        Hotel hotel = hotelService.getById(36934L);
//        2.转换为文档类型
        HotelDoc hotelDoc = new HotelDoc(hotel);
//        3.将hotelDoc转化为JSOn
        String toJSONString = JSON.toJSONString(hotelDoc);
//            准备requst对象
        IndexRequest indexRequest = new IndexRequest("hotel").id(hotelDoc.getId().toString());
//        准备json文档
        indexRequest.source(toJSONString,XContentType.JSON);

//        发送请求

        client.index(indexRequest,RequestOptions.DEFAULT);

    }

    /**
     * 查询文档
     * @throws IOException
     */
    @Test
    void testGetDocumentById() throws IOException {
        // 1.准备Request
        GetRequest request = new GetRequest("hotel", "36934");
        // 2.发送请求，得到响应
        GetResponse response = client.get(request, RequestOptions.DEFAULT);
        // 3.解析响应结果
        String json = response.getSourceAsString();

        HotelDoc hotelDoc = JSON.parseObject(json, HotelDoc.class);
        System.out.println(hotelDoc);
    }

    /**
     * 删除文档
     * @throws IOException
     */
    @Test
    void testDeleteDocument() throws IOException {
        DeleteRequest deleteRequest = new DeleteRequest("hotel","36934");

        client.delete(deleteRequest,RequestOptions.DEFAULT);


    }

    /**
     * 修改文档
     */
    @Test
    void testUpdateDocument() throws IOException {
        UpdateRequest updateRequest = new UpdateRequest("hotel","36934");

        updateRequest.doc("price","999",
                            "starName","五钻");

        client.update(updateRequest, RequestOptions.DEFAULT);
    }
    @Test
    void testBulkRequest() throws IOException {

        List<Hotel> list = hotelService.list();

        BulkRequest bulkRequest = new BulkRequest();

        for (Hotel hotel : list) {
            HotelDoc hotelDoc = new HotelDoc(hotel);

            String s = JSON.toJSONString(hotelDoc);

            IndexRequest indexRequest = new IndexRequest("hotel").id(hotelDoc.getId().toString());

            indexRequest.source(s,XContentType.JSON);

            bulkRequest.add(indexRequest);

        }

        BulkResponse response = client.bulk(bulkRequest, RequestOptions.DEFAULT);

        System.out.println(response);


    }




}
