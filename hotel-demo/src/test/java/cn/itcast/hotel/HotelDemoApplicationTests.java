package cn.itcast.hotel;

import cn.itcast.hotel.constants.HotelConstants;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.common.xcontent.XContentType;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;

@SpringBootTest
class HotelDemoApplicationTests {

    @Autowired
    private RestHighLevelClient client;


    @Test
    void contextLoads() {
        System.out.println(client);
    }

    @Test
    void createHotelIndex() throws IOException {
        // 1.创建requset对象
        CreateIndexRequest createIndexRequest = new CreateIndexRequest("hotel");

        // 2 准备请求参数 DSL语句
        createIndexRequest.source(HotelConstants.MAPPING_TEMPLATE, XContentType.JSON);
        // 3. 发送请求
        client.indices().create(createIndexRequest, RequestOptions.DEFAULT);

    }





}
