package cn.itcast.hotel.service.impl;

import cn.itcast.hotel.mapper.HotelMapper;
import cn.itcast.hotel.pojo.Hotel;
import cn.itcast.hotel.pojo.HotelDoc;
import cn.itcast.hotel.pojo.qo.PageParams;
import cn.itcast.hotel.pojo.vo.PageResult;
import cn.itcast.hotel.service.IHotelService;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.lucene.queries.function.FunctionScoreQuery;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.geo.GeoPoint;
import org.elasticsearch.common.unit.DistanceUnit;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.functionscore.FunctionScoreQueryBuilder;
import org.elasticsearch.index.query.functionscore.ScoreFunctionBuilder;
import org.elasticsearch.index.query.functionscore.ScoreFunctionBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;

@Service
@RequiredArgsConstructor
@Slf4j
public class HotelService extends ServiceImpl<HotelMapper, Hotel> implements IHotelService {

    private final RestHighLevelClient restHighLevelClient;


    @Override
    public PageResult<HotelDoc> search(PageParams pageParams) {

        SearchResponse response = null;
        try {
            SearchRequest searchRequest = new SearchRequest("hotel");
                // 酒店结果过滤，由于查询条件复杂，封装成一个函数
            buildBaseQuery(pageParams,searchRequest);

//            String key = pageParams.getKey();
//            if (key == null || "".equals(key)) {
//
//                searchRequest.source().query(QueryBuilders.matchAllQuery());
//            }else {
//                searchRequest.source().query(QueryBuilders.matchQuery("all",key));
//            }

//        分页
            int page = pageParams.getPage();
            int size = pageParams.getSize();
            searchRequest.source().from((page - 1)*size).size(size);

            // 排序
            String pageParamsLocation = pageParams.getLocation();
            if (pageParamsLocation != null && !pageParamsLocation.equals("")) {
                searchRequest.source().sort(SortBuilders
                        .geoDistanceSort("location", new GeoPoint(pageParamsLocation))
                        .order(SortOrder.ASC)
                        .unit(DistanceUnit.KILOMETERS)
                );
            }


            // 发送请求
            response = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
            // 解析
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        SearchHits searchHits = response.getHits();
        long value = searchHits.getTotalHits().value;
        log.info("总条数：{}",value);
        SearchHit[] hits = searchHits.getHits();
        ArrayList<HotelDoc> hotels = new ArrayList<>();

        for (SearchHit hit : hits) {
            String sourceAsString = hit.getSourceAsString();
            System.out.println(sourceAsString);
            HotelDoc hotelDoc = JSON.parseObject(sourceAsString, HotelDoc.class);
            // 获取排序值
            Object[] sortValues = hit.getSortValues();
            if (sortValues.length>0){
                Object sortValue = sortValues[0];
                hotelDoc.setDistance(sortValue);
            }



            hotels.add(hotelDoc);
        }
        return new PageResult(value,hotels);
    }

    private void buildBaseQuery(PageParams pageParams, SearchRequest searchRequest) {
//        过滤需要用到布尔查询，构建布尔查询
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        String key = pageParams.getKey();
        //精准查询
        if (key == null || key.equals("")){
            boolQueryBuilder.must(QueryBuilders.matchAllQuery());
        }else {
            boolQueryBuilder.must(QueryBuilders.matchQuery("all",key));
        }
        // 城市条件
        if (pageParams.getCity() != null && !pageParams.getCity().equals("")){
            boolQueryBuilder.filter(QueryBuilders.termQuery("city",pageParams.getCity()));
        }
        // 品牌条件
        if (pageParams.getBrand() != null && !pageParams.getBrand().equals("")){
            boolQueryBuilder.filter(QueryBuilders.termQuery("brand",pageParams.getBrand()));
        }
        //星级条件
        if (pageParams.getStarName() != null && !pageParams.getStarName().equals("")){
            boolQueryBuilder.filter(QueryBuilders.termQuery("starName",pageParams.getStarName()));
        }

        // 价格过滤
        if (pageParams.getMinPrice() != null && pageParams.getMaxPrice() != null){
            boolQueryBuilder.filter(QueryBuilders.rangeQuery("price")
            .gte(pageParams.getMinPrice())
            .lte(pageParams.getMaxPrice())
            );

        }

        // 算分控制
        FunctionScoreQueryBuilder functionScoreQueryBuilder = QueryBuilders.functionScoreQuery(
                boolQueryBuilder,
                new FunctionScoreQueryBuilder.FilterFunctionBuilder[]{
                        new FunctionScoreQueryBuilder.FilterFunctionBuilder(
                                QueryBuilders.termQuery("isAD", true),
                                ScoreFunctionBuilders.weightFactorFunction(10)

                        )
                });
        searchRequest.source().query(functionScoreQueryBuilder);
        searchRequest.source().query(boolQueryBuilder);
    }
}
