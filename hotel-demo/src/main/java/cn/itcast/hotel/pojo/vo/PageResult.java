package cn.itcast.hotel.pojo.vo;

import cn.itcast.hotel.pojo.HotelDoc;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PageResult<H> {
    private Long total;
    private List<HotelDoc> hotels;
}
